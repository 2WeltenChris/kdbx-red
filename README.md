# kdbx-RED

This node lets you manage kdbx files (as used by KeePass and others). It is based on [kdbxweb](https://github.com/keeweb/kdbxweb).

> **⚠️ Warning: ⚠️ Use this node with care! Opening a kdbx file with this node exposes the contained credentials to the flow (and thus to the memory). Therefore you should only use it in a trusted environment.**
>
> It is probably safer than using a badly secured database or a plain text file on disk. But you still need to be careful. Always remember: **You use it on your own risk! So use it with care!**
> 
> We use it ourselves to authenticate users in a web-app (either by having one kdbx file for each user and auth fails in case the kdbx-database could not be opened; or by having one kdbx file for all users and the credentials used for authorization are within this file). Having further credentials within the kdbx-database, the flow may also use services the user would normally not have access to. Thus this node allows to hide credentials from the user that he should not have access to while he still should use e.g. a certain web service from within the app.

## Status

Currently you can:

- create or delete databases
- create, update or delete groups
- create, update or delete entries, list all entries or get a specific entry

## Supported kdbx versions

Currently it works best with kdbx version 3.1 but also kdbx version 4 (argon 2) is supported. Older kdbx files are not supported! Please mind that kdbx version 4 files can be a bit slower. If you use the `open database` operation, the database will be kept open and cached to the node.context so that further operations are faster. Please remember to save the database later using the `close database` operation in order to persist any changes.

## Configuration

You can put the data into the msg.payload (get more details on this through the information panel) or configurate it from within the node. 

You can select your entry or group by name or by uuid. The root group is selected either by an empty string or using `/`. For subfolder selection use `/` as a separator. For example `my first folder/subfolder`.

It's best to use the uuid to select an entry or a group as otherwise a searched is performed and the first matching entry or group will be returned.

Most times all parameters are mandatory. Anyway some operations like e.g. the `update entry` operation need information on which parameters should be processed. Thus you have a checkbox to select these parameters.
### Node configuration
![Node](https://gitlab.com/2WeltenChris/kdbx-red/raw/master/examples/node_configuration.png?raw=true "Node Configuration")
### File node configuration
![File Node](https://gitlab.com/2WeltenChris/kdbx-red/raw/master/examples/node_file_configuration.png?raw=true "File Node Configuration")

## Sample flow

You can find a sample flow in the node red imports where you can see some examples on how to configure the node.

![Example](https://gitlab.com/2WeltenChris/kdbx-red/raw/master/examples/examples.png?raw=true "Example Node")
