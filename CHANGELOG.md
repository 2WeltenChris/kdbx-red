# Changelog

**Attention:** ⚠️ means that a change may breaks things. Manual adjustments will be necessary. So be careful before updating. Even data loss might occur.

## Version 1.3.2 (November 21st 2024)
- contextVar name fix
- refactored task

## Version 1.3.1 (November 20th 2024)
- change db password fix
- missing eval property keys fix

## Version 1.3.0 (July 22nd 2024)
- refactored code
- renamend some properties
- added warning for string saved pw / new pw input
- removing not needed criticial parameters on saving
- remove garbage on saving
- latest SIR version

## Version 1.2.3 (April 22nd 2024)
- fixed node-red evaluation for NR 4.0

## Version 1.2.2 (April 18th 2024)
- changed argon error handling
- add error status on node

## Version 1.2.0 (January 31th 2024)
- ⚠️config node: filepath and key filepath are no longer credentials -> credential still active for this version, on opening/closing node values will be moved. Credentials for paths will be removed in further version.
- SIR update
- config node created with SIR
- config node password can be a fixed credential string or a variable (msg/env)
- debugMode -> log errors on server
- find group fix
- getValue removed legacy msg.payload direct usage

## Version 1.1.1 (January 12th 2024)
- getGroup fix

## Version 1.1.0 (January 12th 2024)
- ⚠️ other output error message will be set into msg.payload
- server side stack trace logging only in dev mode

## Version 1.0.3 (January 12th 2024)
- Bugfix get/set entry password

## Version 1.0.2 (January 11th 2024)
- Better error handling to prevent crashes
- evaluate only needed parameters

## Version 1.0.1 (January 10th 2024)
- Bugfix using relative path

## Version 1.0.0
 - ⚠️ Added new properties for dynamic file usage
 - msg.kdbx is depreceated (but can now be set in the node)
 - Internal restructuring

## Version 0.2.0
 - Task added: Get group
 - Updated to latest SIR version

## Version 0.1.8
 - Use password field for new entries if no providing field was found.

## Version 0.1.7
 - Error handling for missing password field
 - set kdbxweb version fix

## Version 0.1.6
  - Bugfix password
  - Bugfix UI Handling update group

## Version 0.1.5
  - Bugfix get password
  - Created with SIR
  - Errorhandling fix
  - And some other small changes

## Version 0.1.4
 - Updated to kdbxweb 2.1.1

## Version 0.1.3
- Bugfix Error if msg.payload was not an object

## Version 0.1.2
- Added git repository tag
- Added security warning