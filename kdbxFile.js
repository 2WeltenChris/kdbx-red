module.exports = function (RED) {
  function KdbxFileNode (config) {
    RED.nodes.createNode(this, config)
    this.filePath = config.filePath
    this.keyFilePath = config.keyFilePath
    this.typedInputPassword = config.typedInputPassword
    this.typedInputPasswordType = config.typedInputPasswordType
  }
  RED.nodes.registerType('kdbxFile', KdbxFileNode, {
    credentials: {
      password: { type: 'password' },
      filePath: { type: 'text' }, // legacy, will be removed within the next version -> moving to non credential property
      keyFilePath: { type: 'text' } // legacy, will be removed within the next version -> moving to non credential property
    }
  })
}
