const dbHelper = require('./utils/databaseHelper.js')
const entriesHelper = require('./utils/entriesHelper.js')
const { createParameters } = require('./utils/helpers.js')
const util = require('util')

module.exports = function (RED) {
  const evalProp = util.promisify(RED.util.evaluateNodeProperty)
  function KdbxManager (config) {
    RED.nodes.createNode(this, config)
    const node = this
    let fileData = {}
    const contextFlow = node.context().flow
    const showNoStatus = ['openDb', 'listEntries', 'getGroup', 'getEntry', 'deleteDb']

    const setNodeStatus = (error='') => {
      if (error) {
        node.status({ fill: 'error', shape: 'dot', text: error.substring(0, 35) })
      } else if (config.task && config.saveDb && !showNoStatus.includes(config.task)) {
        node.status({ fill: 'green', shape: 'dot', text: 'save db' })
      } else {
        node.status({})
      }
    }
    setNodeStatus()

    node.on('input', async function (msg, send, done) {
      setNodeStatus()

      const handleError = (error) => {
        // remove password from msg object when dynamic file access
        if (config.dynamicFile && config.parameterInputType.filePassword === 'msg') {
          const objectPathArray = config.filePassword.split('.')
          const lastKey = objectPathArray.pop()
          let pwPointer = msg
          objectPathArray.forEach(key => pwPointer = pwPointer[key])
          pwPointer[lastKey] = '[removed]'
        }
        // set status
        setNodeStatus(typeof error === 'string' ? error : error?.message || 'Error')
        // add kdbx prefix
        if (typeof error === 'string' && !error.startsWith('[kdbx-red]')) {
          error = '[kdbx-red] ' + error
        } else if (error.message && !error.message.startsWith('[kdbx-red]')) {
          error.message = '[kdbx-red] ' + error.message
        }
        if (config.debugMode) {
          console.error(error) // show stack trace to server console
        }
        node.status({ fill: 'red', shape: 'dot', text: 'Error' })
        if (config.errorHandling === "other output") {
          // node-red does not send the whole error message by send. Therefor a new "error" object
          let errorMsg = {
            error: true,
            message: typeof error === 'string' ? error : error.message,
            stack: error.stack,
            source: {
              id: node.id,
            type: node.type
            }
          }
          msg.payload = errorMsg
          send([null, msg])
          done()
        } else {
          // done must be only a string or a real error object (else it returns [Object object])
          done(error)
        }
      }

      try {
        // collect metadata
        if (config.dynamicFile) {
          fileData = {}
          fileData.file = await evalProp(config.filePath, config.parameterInputType.filePath, node, msg)
          if (config.parameterInputType.keyFilePath === 'no input') {
            fileData.keyFile = ''  
          } else {
            fileData.keyFile = await evalProp(config.keyFilePath, config.parameterInputType.keyFilePath, node, msg)
          }
          if (config.parameterInputType.filePassword === 'no input') {
            fileData.password = ''  
          } else {
            fileData.password = await evalProp(config.filePassword, config.parameterInputType.filePassword, node, msg)
          }
        } else {
          // using static config file
          const kdbxFile = RED.nodes.getNode(config.kdbxFile)
          fileData.file = kdbxFile.filePath || kdbxFile.credentials.filePath // credential option is legacy
          fileData.keyFile = kdbxFile.keyFilePath || kdbxFile.credentials.keyFilePath // credential option is legacy
          if (kdbxFile.credentials.password) {
            fileData.password = kdbxFile.credentials.password
          } else if(kdbxFile.typedInputPassword) {
            fileData.password = await evalProp(kdbxFile.typedInputPassword, kdbxFile.typedInputPasswordType, kdbxFile, msg)
          }
        }

        if (!fileData.file) {
          throw new Error('No file stated.')
        }
        if (!fileData.file.endsWith('.kdbx')) {
          fileData.file = fileData.file + '.kdbx'
        }

      
        // check if database is locally stored in contextFlow (by open Task) else it will be opened for a one use operation
        const parameters = await createParameters(evalProp, msg, config)
        // contextFlow variables must not include "." or blank.
        const contextVarName = fileData.file.replaceAll('.', '_').replaceAll(' ', '_')
        let db = contextFlow.get(contextVarName)
        const specialTasks = ['openDb', 'closeDb', 'createDb', 'deleteDb']

        if (!db && !specialTasks.includes(parameters.task)) {
          db = await dbHelper.open(fileData)
        }
        // now handle the task
        let dbHasChanged = false
        switch (parameters.task) {
          case 'openDb':
            if (contextFlow.get(contextVarName)) {
              throw new Error('Database "' + fileData.file + '" already opened!')
            } else {
              const thisDb = await dbHelper.open(fileData)
              contextFlow.set(contextVarName, thisDb)
              msg.payload = 'Database "' + fileData.file + '" opened.'
            }
            break
          case 'createDb':
            msg.payload = await dbHelper.create(fileData, parameters.setKdbx4)   
            break
          case 'deleteDb':
            await dbHelper.deleteFile(fileData, parameters.deleteKeyFile)
            if (parameters.deleteKeyFile && fileData.keyFile) {
              msg.payload = 'Database "' + fileData.file + '" and Keyfile "' + fileData.keyFile + '" deleted.'
            } else {
              msg.payload = 'Database "' + fileData.file + '" deleted.'
            } 
            break
          case 'updateDb':
            await dbHelper.changePw(parameters.password, db)
            dbHasChanged = true
            msg.payload = 'Database "' + fileData.file + '" updated.'
            break
          case 'closeDb':
            if (!db) {
              throw new Error('No database opened!')
            } else {
              dbHasChanged = true
              contextFlow.set(contextVarName, undefined)
              msg.payload = 'Database "' + fileData.file + '" closed.'
            }
            break
          case 'listEntries':
            const group = await entriesHelper.getGroup(db, parameters)
            if (!group.parentGroup) {
              group.groups = group.groups.filter(g => g.name !== 'Recycle Bin')
            }
            msg.payload = await entriesHelper.listGroupEntries(group)
            break
          case 'createEntry':
            const newEntry = await entriesHelper.createEntry(db, parameters)
            dbHasChanged = true
            msg.payload = await entriesHelper.getEntryInfo(newEntry)
            break
          case 'getEntry': 
            const entry = await entriesHelper.getEntry(db, parameters)
            msg.payload = await entriesHelper.getEntryInfo(entry)
            break
          case 'updateEntry':
            const updateEntry = await entriesHelper.updateEntry(db, parameters)
            dbHasChanged = true
            msg.payload = await entriesHelper.getEntryInfo(updateEntry)
            break
          case 'deleteEntry':
            await entriesHelper.deleteEntry(db, parameters)
            dbHasChanged = true
            msg.payload = 'Entry "' + parameters.entry + '" deleted'
            break
          case 'createGroup':
            const newGroup = await entriesHelper.createGroup(db, parameters)
            dbHasChanged = true
            msg.payload = 'Group "' + newGroup.name + '" created'
            break
          case 'getGroup':
            msg.payload = await entriesHelper.getGroup(db, parameters)
            break
          case 'updateGroup':
            await entriesHelper.updateGroup(db, parameters)
            dbHasChanged = true
            msg.payload = 'Group "' + parameters.group + '" updated to params.newGroup'
            break
          case 'deleteGroup':
            await entriesHelper.deleteGroup(db, parameters)
            dbHasChanged = true
            msg.payload = 'Group "' + parameters.group + '" deleted'
            break
          default:
            throw new Error('Invalid task')
        }

        // shall we save changes made to the db?
        if (parameters.saveDb && dbHasChanged) {
          await dbHelper.save(fileData.file, db)
        }
        send(msg)
        return done()
      } catch (e) {
        return handleError(e)
      }
    })
  }
  RED.nodes.registerType('kdbx', KdbxManager)
}
