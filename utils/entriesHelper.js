const kdbxweb = require('kdbxweb')

const createGroup = async (db, params) => {
  // set by msg.kdbx
  if (!params.group && params.newGroup) {
    params.group = params.newGroup 
  }
  if (params.group.endsWith("/")) {
    params.group = params.group.slice(0, -1)
  }
  let orgParamsGroup = params.group
    
  // we change param.groups to it's parent to check if group exists
  let parentGroups = params.group.split('/')
  let newGroupname = parentGroups.pop()
  let i = 0
  let groupString = ''
  while (parentGroups.length > 0 && i < parentGroups.length) {
    let currentGroup = parentGroups[i]
    if (groupString) {
      groupString += '/' + currentGroup
    } else {
      groupString = currentGroup
    }
    params.group = groupString
    let groupExists = await getGroup(db, params, false)
    if (!groupExists) {
      // create in root or in other group
      if (i === 0) {
        db.createGroup(db.getDefaultGroup(), currentGroup)
      } else {
        let parent = groupString.split('/')
        parent.pop()
        parent = parent.join('/')
        params.group = parent
        let thisParentGroup = await getGroup(db, params)
        db.createGroup(thisParentGroup, currentGroup)
      }
    }
    i++
  }
  // try to create last group
  params.group = parentGroups.join('/')
  parentGroups = await getGroup(db, params, false)

  if (parentGroups) {
    const groupExists = parentGroups.groups.filter(group => group.name === newGroupname)[0]
    if (groupExists) {
      throw new Error('Group "' + orgParamsGroup + '" exists already!')
    }
  } else {
    parentGroups = db.getDefaultGroup()
  }
  const newGroup = db.createGroup(parentGroups, newGroupname)
  return newGroup
}

const updateGroup = async (db, params) => {
  const group = await getGroup(db, params)
  group.name = params.newGroup
}

const deleteGroup = async (db, params) => {
  const group = await getGroup(db, params)
  if (group.uuid.id === db.getDefaultGroup().uuid.id) {
    throw new Error("Can't delete root group")
  }
  db.remove(group)
}

const getGroup = async (db, params, mustExist = true) => {
  const rootGroup = db.getDefaultGroup()
  // params.group can be an object if returned from a getGroup command
  const thisGroupUuid = params.group.uuid || params.group
  if (!params.group || params.group === '/' || rootGroup.uuid.id === thisGroupUuid) {
    return rootGroup // return root directory
  }

  // search for this group by id
  let group = rootGroup.groups.find((g) => g?.uuid?.id === thisGroupUuid)
  if (group) {
    return group
  }

  // group is a path
  let groupArray = params.group.split('/')
  // remove empty subgroups due to beginning and ending "/" (for example (root)/subgroup/ => subgroup)
  groupArray = groupArray.filter((element) => element !== '')
  group = rootGroup
  for (const groupname of groupArray) {
    const nextGroup = group.groups.filter(group => group.name === groupname)
    if (nextGroup.length > 0) {
      // use getGroup(uuid), otherwise we are missing some info in the group object (like parent group)
      group = db.getGroup(nextGroup[0].uuid) // we take the first hit, don't build groups in the same group with same name :)
    } else {
      group = undefined
      break
    }
  }
  if (!group && mustExist) {
    throw new Error('Group "' + params.group + '" not found')
  }
  return group    
}

const getEntry = async (db, params) => {
  const group = await getGroup(db, params)
  let foundEntry = group.entries.find(entry => entry.uuid.id === params.entry)
  if (!foundEntry) {
    foundEntry = group.entries.find(entry => entry.fields.get("Title") === params.entry)
  }
  if (foundEntry) {
    return foundEntry
  } else {
    throw new Error('Title "' + params.entry + '" not found')
  }  
}

const createEntry = async (db, params) => {
  const group = await getGroup(db, params)
  const entry = db.createEntry(group)
  entry.fields.set('Title', params.entry)
  entry.fields.set('UserName', params.username)
  if (entry.fields.has('Password')) {
    entry.fields.set('Password', kdbxweb.ProtectedValue.fromString(params.password))
  } else if (entry.fields.has('Pin')) {
    entry.fields.set('Pin', kdbxweb.ProtectedValue.fromString(params.password))
  } else {
    // fallback, as password should be the standard field https://keepass.info/help/base/fieldrefs.html
    entry.fields.set('Password', kdbxweb.ProtectedValue.fromString(params.password))
  }
  entry.fields.set('URL', params.url)
  entry.fields.set('Notes', params.notes)
  return entry
}

const updateEntry = async (db, params) => {
  const entry = await getEntry(db, params)
  entry.pushHistory()
  if (params.newEntryTitle) entry.fields.set('Title', params.newEntryTitle)
  if (params.newUsername) entry.fields.set('UserName', params.newUsername)
  if (params.newPassword) {
    if (entry.fields.has('Password')) {
      entry.fields.set('Password', kdbxweb.ProtectedValue.fromString(params.newPassword))
    } else if (entry.fields.has('Pin')) {
      entry.fields.set('Pin', kdbxweb.ProtectedValue.fromString(params.newPassword))
    } else {
      // fallback, as password should be the standard field https://keepass.info/help/base/fieldrefs.html
      entry.fields.set('Password', kdbxweb.ProtectedValue.fromString(params.password))
    }
  }    
  if (params.newUrl) entry.fields.set('URL', params.newUrl)
  if (params.newNotes) entry.fields.set('Notes', params.newNotes)

  // TODO get entry group by UUID and check with UUID from group, if different => move
  if (typeof params.newGroup !== 'undefined') {
    params.group = params.newGroup
    const newGroup = await getGroup(db, params)
    db.move(entry, newGroup)
  }
  return entry
}

const deleteEntry = async (db, params) => {
  const entry = await getEntry(db, params)
  db.remove(entry)
}

const listGroupEntries = async (group, entries = []) => {
  for (const entry of group.entries) {
    entries.push(await getEntryInfo(entry))
  }
  for (const subGroup of group.groups) {
    listGroupEntries(subGroup, entries)
  }
  return entries
}

const getEntryInfo = async (entry) => {
  const fields = entry.fields
  // password can be stored in Pin? There was a short time where Password was undefined and it was in Pin...
  // kdbxweb is now fixed version
  let password = '[kdbx-red] no password field found'
  if (fields.has('Password')) {
    // empty password field cannot use getText()
    password = fields.get('Password') === '' ? '' : fields.get('Password').getText()
  } else if (fields.has('Pin')) {
    password = fields.get('Pin') === '' ? fields.get('Pin') : fields.get('Pin').getText()
  }

  return {
    title: fields.get('Title'),
    username: fields.get('UserName'),
    password,
    url: fields.get('URL'),
    notes: fields.get('Notes'),
    uuid: entry.uuid.id,
    group: {
      name: entry.parentGroup.name,
      uuid: entry.parentGroup.uuid.id
    }
  }
}

module.exports = {
  getGroup,
  createGroup,
  updateGroup,
  deleteGroup,
  getEntry,
  listGroupEntries,
  getEntryInfo,
  createEntry,
  updateEntry,
  deleteEntry
}
