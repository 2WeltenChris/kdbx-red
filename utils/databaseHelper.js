const kdbxweb = require('kdbxweb')
const fs = require('fs')

// TODO: Optimize Argon2
kdbxweb.CryptoEngine.argon2 = function (password, salt, memory, iterations, length, parallelism, type, version) {
  try {
  const Module = require('./argon2-asm.min')
  const passwordLen = password.byteLength
  password = Module.allocate(new Uint8Array(password), 'i8', Module.ALLOC_NORMAL)
  const saltLen = salt.byteLength
  salt = Module.allocate(new Uint8Array(salt), 'i8', Module.ALLOC_NORMAL)
  const hash = Module.allocate(new Array(length), 'i8', Module.ALLOC_NORMAL)
  const encodedLen = 512
  const encoded = Module.allocate(new Array(encodedLen), 'i8', Module.ALLOC_NORMAL)
  // jshint camelcase:false
    const res = Module._argon2_hash(iterations, memory, parallelism, password, passwordLen, salt, saltLen, hash, length, encoded, encodedLen, type, version)
    if (res) {
      throw new Error('[kdbx-red] Argon2 create hash error.')
    }
    const hashArr = new Uint8Array(length)
    for (var i = 0; i < length; i++) {
      hashArr[i] = Module.HEAP8[hash + i]
    }
    Module._free(password)
    Module._free(salt)
    Module._free(hash)
    Module._free(encoded)
    return Promise.resolve(hashArr)
  } catch (e) {
    // prevent returning the whole argon file as error.
    return Promise.reject('[kdbx-red] Argon2 create hash error.')
  }
}

const create = async (fileData, setKdbx4) => {
  if (!fileData.databaseName) {
    fileData.databaseName = 'New database'
  }
  if (fs.existsSync(fileData.file)) {
    throw new Error('Database ' + fileData.file + ' already exists')
  }

  let keyFileBuffer = ''
  if (fileData.keyFile) {
    if (fs.existsSync(fileData.keyFile)) {
      keyFileBuffer = fs.readFileSync(fileData.keyFile)
    } else {
      keyFileBuffer = await kdbxweb.Credentials.createRandomKeyFile()
      fs.writeFileSync(fileData.keyFile, keyFileBuffer)
    }
  }
  if (!fileData.password) {
    fileData.password = ''
  }
  const credentials = new kdbxweb.Credentials(kdbxweb.ProtectedValue.fromString(fileData.password), keyFileBuffer)
  const newDb = kdbxweb.Kdbx.create(credentials, fileData.name)
  if (setKdbx4) newDb.upgrade()
  await save(fileData.file, newDb)
  return 'Database "' +  fileData.file + '" created'
}

const open = async (fileData) => {
  // make sure file will be an ArrayBuffer, not just a Buffer Object
  const fileArrayBuffer = new Uint8Array(fs.readFileSync(fileData.file)).buffer
  const keyFileBuffer = fileData.keyFile ? fs.readFileSync(fileData.keyFile) : ''
  const credentials = new kdbxweb.Credentials(kdbxweb.ProtectedValue.fromString(fileData.password), keyFileBuffer)
  const db = kdbxweb.Kdbx.load(fileArrayBuffer, credentials)
  return db
}

const save = async (filePath, db) => {
  const dbFile = await db.save()
  fs.writeFileSync(filePath, Buffer.from(dbFile))
}

const deleteFile = async (fileData = '', deleteKeyFile = '') => {
  // check if file(s) exists
  if (!fs.existsSync(fileData.file)) {
    throw new Error('Database file: "' + fileData.file + '" not found!')
  }
  if (deleteKeyFile && fileData.keyFile && !fs.existsSync(fileData.file)) {
    throw new Error('Keyfile "' + fileData.file + '" not found!')
  } 
  // delete files
  fs.unlinkSync(fileData.file)
  if (deleteKeyFile && fileData.keyFile) {
    fs.unlinkSync(fileData.keyFile)
  }
}

const changePw = async (newPw, db) => {
  await db.credentials.setPassword(kdbxweb.ProtectedValue.fromString(newPw))
}

module.exports = {
  create,
  open,
  save,
  deleteFile,
  changePw
}
