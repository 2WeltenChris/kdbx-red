const createParameters = async (evalProp, msg, config) => {
  const result = {
    task: config.task
  } 
  let parametersToAdd = []

  switch (result.task) {
    case 'createDb':
      parametersToAdd.push('setKdbx4')
      break
    case 'openDb': 
      // add nothing
      break
    case 'closeDb':
      parametersToAdd.push('saveDb')
      break
    case 'updateDb':
      parametersToAdd = ['password', 'saveDb']
      break
    case 'deleteDb':
      parametersToAdd.push('deleteKeyFile')
      break
    case 'getGroup':
    case 'listEntries':
      parametersToAdd.push('group')
      break
    case 'createGroup':
    case 'deleteGroup':
      parametersToAdd = ['group', 'saveDb']
      break
    case 'updateGroup':
      parametersToAdd = ['group', 'newGroup', 'saveDb']
      break
    case 'createEntry':
      parametersToAdd = ['group', 'entry', 'username', 'password', 'url', 'notes', 'saveDb']
      break
    case 'getEntry':
      parametersToAdd = ['group', 'entry']
      break
    case 'updateEntry':
      parametersToAdd = ['group', 'entry', 'saveDb']
      const updatableParams = ['newGroup', 'newEntryTitle', 'newUsername', 'newPassword', 'newUrl', 'newNotes']
      updatableParams.forEach(p => {
        if (config.updateParameters[p]) {
          parametersToAdd.push(p)
        }
      })
      break
    case 'deleteEntry':
      parametersToAdd = ['group', 'entry', 'saveDb']
      break
    default:
      throw new Error('[kdbx-red] Invalid task: "' + task + '".')
  }

  // evaluate the real value of the parameters (string, msg, flow,...)
  for (const param of parametersToAdd) {
    if (param === 'deleteKeyFile' || param === 'saveDb') {
      result[param] = config[param]
    } else {
      result[param] = await evalProp(config[param], config.parameterInputType[param], null, msg)
    }
}
  return result
}

module.exports = {
  createParameters
}