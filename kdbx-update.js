// add temporarily node default keys
// this function is handy if you rename or split old values
const addLegacyProperties = () => {
  const legacyProps = []
  legacyProps.push('parameterIsActive')
  return legacyProps
}

const clientUpdate = (version, node) => {
  if (version.aggregated < version.parse('1.0.0').aggregated) {
    // init first update to v 1.0.0
    node.dynamicFile = false
    node.parameterInputType.filePath = 'msg'
    node.parameterInputType.keyFilePath = 'msg'
    node.parameterInputType.filePassword = 'msg'
    node.filePath = "payload.path"
    node.keyFilePath = "payload.keyPath"
    node.filePassword = "payload.password"
    node.errorHandling = "throw error"
  }

  if (version.aggregated < version.parse('1.2.0').aggregated)  {
    node.debugMode = false
  }

  if (version.aggregated < version.parse('1.2.4').aggregated) {
    // renamend parameterIsActive -> updateParameters
    node.updateParameters = {}
    const updateKeys = ['newGroup', 'newEntryTitle', 'newUsername', 'newPassword', 'newUrl', 'newNotes']
    Object.keys(node.parameterIsActive).forEach(k => {
      // only add true values
      if (updateKeys.includes(k) && node.parameterIsActive[k]) {
        node.updateParameters[k] = true
      }
    })
    delete node.parameterIsActive
  }

  return node
}

module.exports = { clientUpdate, addLegacyProperties }