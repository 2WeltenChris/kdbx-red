#!/usr/bin/env bash
# Update SIR
cd /workspace/kdbx-red
npm update -g svelte-integration-red
npm update svelte-integration-red
# Install dependencies
npm install
# Link workspace to node-red
mkdir -p /home/gitpod/.node-red
cd /home/gitpod/.node-red/
npm install
npm link /workspace/kdbx-red --save
# Go back to workspace directory
cd /workspace/kdbx-red